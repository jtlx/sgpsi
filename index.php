<!-- Q: What did Indonesia say to Singapore? -->
<!-- A: Heyz! -->

<!DOCTYPE html>
<html>
<head>
    <title>Thanks Indonesia</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" charset="utf-8">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
    <meta name="keywords" content="PSI, graph, chart, singapore, air">
    <meta name="description" content="Singapore's PSI visualised">

</head>

<body>


    <?php
    // Defining the basic cURL function
    function curl($url) {
        $ch = curl_init();  // Initialising cURL
        curl_setopt($ch, CURLOPT_URL, $url);    // Setting cURL's URL option with the $url variable passed into the function
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // Setting cURL's option to return the webpage data
        $data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
        curl_close($ch);    // Closing cURL
        return $data;   // Returning the data from the function
    }
    ?>

    <?php
    // Defining the basic scraping function
    function scrape_between($data, $start, $end){
        $data = stristr($data, $start); // Stripping all data from before $start
        $data = substr($data, strlen($start));  // Stripping $start
        $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
        $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
        return $data;   // Returning the scraped data from the function
    }
    ?>

    <?php
    date_default_timezone_set('Asia/Singapore');
   // $scraped_page = curl("http://dashsell.com/sgpsi");    // Downloading IMDB home page to variable $scraped_page
   // $scraped_data = scrape_between($scraped_page, "<div class=\"psinow\">", "</div>");   // Scraping downloaded dara in $scraped_page for content between <title> and </title> tags 


 
    //echo htmlspecialchars($scraped_data); // Echoing $scraped data


    //echo "<br> "; ////////////////////////////////////REMOVE AFTER TESTING
    //echo date("gA");////////////////////////////////REMOVE AFTER TESTING


    // if (date("gA") == "3AM")
    // {
    //     echo "yay";
    // }

    $con=mysqli_connect("localhost","psi","ma1ebc77651","psi"); //use for local testing
    //$con=mysqli_connect("mysql6.000webhost.com","a7635645_user","ma1ebc77651","a7635645_psi"); //$host, $user, $pass, $db

    if (mysqli_connect_errno($con))
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $time = date("gA");

    $result = mysqli_query($con,"SELECT time FROM psi ORDER BY timerec DESC  LIMIT  1");
    $last_update = mysqli_fetch_array($result);

    if($time != $last_update[0] && intval(date("i")) > 20) //if new hour and 10 mins then update
    {
        $scraped_page = curl("http://app2.nea.gov.sg/anti-pollution-radiation-protection/air-pollution/psi/psi-readings-over-the-last-24-hours");    // Downloading IMDB home page to variable $scraped_page
        $scraped_data = scrape_between($scraped_page, "3-hr PSI Readings", "<div class=\"sfContentBlock\">");   // Scraping downloaded dara in $scraped_page for content between <title> and </title> tags 
        $psi = scrape_between($scraped_data, "<strong style=\"font-size:14px;\">", "</strong>");
        echo htmlspecialchars($psi);
        mysqli_query($con,"INSERT INTO psi (time,PSI)
        VALUES ('$time',$psi)");
    }


    $psis = array();



    $psi_stats = mysqli_query($con,"SELECT PSI 
                                            FROM (
                                                SELECT * 
                                                FROM psi
                                                ORDER BY timerec DESC 
                                                LIMIT 24
                                            ) AS T
                                            ORDER BY timerec ASC
                                             ");






    while($row = mysqli_fetch_array($psi_stats))
    {
        $psis[] = $row['PSI'];
    }


    $times = array();
    $times_stats = mysqli_query($con,"SELECT time 
                                            FROM (
                                                SELECT * 
                                                FROM psi
                                                ORDER BY timerec DESC 
                                                LIMIT 24
                                            ) AS T
                                            ORDER BY timerec ASC
                                             ");
    while($row = mysqli_fetch_array($times_stats))
    {
        $times[] = $row['time'];
    }


    ?>

    <script type="text/javascript">

        var hours = <?php echo json_encode($times); ?>;
        var PSI = <?php echo json_encode($psis); ?>;
        for(var i=0; i<PSI.length; i++) { PSI[i] = parseInt(PSI[i], 10); } 





    </script>

    <script type="text/javascript">



    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'line',
                marginRight: 130,
                marginBottom: 25,
                borderRadius: 0,
                backgroundColor:'rgba(255, 255, 255, 0.7)'            },
            title: {
                text: 'Latest PSI Readings',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: http://www.nea.gov.sg/psi/',
                x: -20
            },
            xAxis: {
                categories: hours 
            },
            yAxis: {
                title: {
                    text: 'PSI'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: '3-Hour Average',
                //data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                data: PSI
            },
            //  {
            //     name: '24-Hour Average',
            //     data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            // }
            ]
        });
    });
    

        </script>
     <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    

    <div id="center-box">
        <div id="container" style="height: 700px; margin: 0 auto"></div>

        <div class="blackbox">
            <div class="blackbox-title"><h3 style="padding-left:5px;">PSI</h3></div>
            <h3 style="text-align:center;  color:orange;"><?php print end($psis); ?></h3>
        </div>
    </div>



<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=9048975; 
var sc_invisible=1; 
var sc_security="b75435a5"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="site stats"
href="http://statcounter.com/free-web-stats/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/9048975/0/b75435a5/1/"
alt="site stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>

</html>
